#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"
#include "disastrOS_constants.h"

void internal_semClose(){

    //ottengo il filedescriptor dalla syscall
    int fd=running->syscall_args[0];

    // dalla lista dei descrittori prendo il descrittore del processo corrente
    SemDescriptor* sem_des= SemDescriptorList_byFd(&running->sem_descriptors,fd);


    if(!sem_des){
        running->syscall_retvalue=DSOS_SEM_DES_NOTFOUND;
        fprintf(stderr, "Error SEM_DES_NOTFOUND: %d\n",DSOS_SEM_DES_NOTFOUND);
        return;
    }

    //rimuovo il descrittore dal PCB
    List_detach(&running->sem_descriptors, (ListItem*) sem_des);

    //prendo il semaforo dal sem_des
    Semaphore* sem=sem_des->semaphore;


    if(!sem){
        running->syscall_retvalue=DSOS_SEM_NOTFOUND;
        fprintf(stderr, "Error SEM_NOTFOUND: %d\n",DSOS_SEM_NOTFOUND);

        return;
    }

    //rimuovo il puntatore per il descrittore corrispondente sem_des
    SemDescriptorPtr* sem_des_ptr=(SemDescriptorPtr*)List_detach(&sem->descriptors, (ListItem*) sem_des->ptr);

    if(!sem_des_ptr){
        running->syscall_retvalue=DSOS_SEM_DES_PTR_NOTFOUND;
        fprintf(stderr, "Error SEM_DES_PTR_NOTFOUND: %d\n",DSOS_SEM_DES_PTR_NOTFOUND);

        return;

    }
    //se non ci sono descrittori in entrambe le liste (e quindi in sem)
    //elimino sem dalla lista dei semafori sem_list
    if(sem->descriptors.size==0 && sem->waiting_descriptors.size==0){
        List_detach(&semaphores_list, (ListItem*)sem);
        Semaphore_free(sem);
    }

    //dealloco risorse e resetto valore di ritorno della syscall
    SemDescriptor_free(sem_des);
    SemDescriptorPtr_free(sem_des_ptr);
    running->syscall_retvalue=0;

}


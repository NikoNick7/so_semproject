#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semWait(){

    int fd=running->syscall_args[0];

    printf("[#%d] SEMWAIT!!!!, fd=%d\n",running->pid,fd);

    SemDescriptor* sem_des= SemDescriptorList_byFd(&running->sem_descriptors, fd);


    if(!sem_des){
        running->syscall_retvalue=DSOS_SEM_DES_NOTFOUND;
        fprintf(stderr, "Error SEM_DES_NOTFOUND: %d\n",DSOS_SEM_DES_NOTFOUND);

        return;

    }

    SemDescriptorPtr* sem_des_ptr=sem_des->ptr;

    if(!sem_des_ptr){
        running->syscall_retvalue=DSOS_SEM_DES_PTR_NOTFOUND;
        fprintf(stderr, "Error DSOS_SEM_DES_PTR_NOTFOUND: %d\n",DSOS_SEM_DES_PTR_NOTFOUND);

        return;


    }

    Semaphore* sem= sem_des->semaphore;


    if(!sem){
        running->syscall_retvalue=DSOS_SEM_NOTFOUND;
        fprintf(stderr, "Error SEM_NOTFOUND: %d\n",DSOS_SEM_NOTFOUND);

        return;


    }

    sem->count=sem->count-1;

    // se count <0 processo corrente viene messo in attesa
    //(viene aggiunto nella waiting list)

    if(sem->count<0){

     //rimuovo sem_des_ptr (processo) nella descriptor list dei semafori

        List_detach(&sem->descriptors, (ListItem*)sem_des_ptr);

     // inserisco processo nella waiting list del semaforo
        List_insert(&sem->waiting_descriptors, sem->waiting_descriptors.last,(ListItem*)sem_des_ptr);

     //inserisco il processo corrente nella waiting list
        List_insert(&waiting_list, waiting_list.last,(ListItem*)running);

        running->status=Waiting;

     //prendo il prossimo processo da schedulare
        PCB* pcb_n= (PCB*)List_detach(&ready_list, ready_list.first);

        running=pcb_n;

    }
    running->syscall_retvalue=0;
    return;

}

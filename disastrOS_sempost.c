#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semPost(){

    int fd= running->syscall_args[0];

    printf("[#%d] SEMPOST!!!!, fd=%d\n",running->pid,fd);

    //descrittore del sem su cui fare semPost
    SemDescriptor* sem_des= SemDescriptorList_byFd(&running->sem_descriptors, fd);

    if(!sem_des){
        running->syscall_retvalue=DSOS_SEM_DES_NOTFOUND;
        fprintf(stderr, "Error SEM_DES_NOTFOUND: %d\n",DSOS_SEM_DES_NOTFOUND);
        return;

    }

    SemDescriptorPtr* sem_des_ptr;

    Semaphore* sem=sem_des->semaphore;

    if(!sem){
        running->syscall_retvalue=DSOS_SEM_NOTFOUND;
        fprintf(stderr, "Error SEM_NOTFOUND: %d\n",DSOS_SEM_NOTFOUND);

        return;


    }


    sem->count++;

    // se count <=0 il processo corrente viene sbloccato
    //(viene aggiunto nella ready list)


    if(sem->count <=0){

        // prendo/tolgo processo dalla waiting list del semaforo
        sem_des_ptr=(SemDescriptorPtr*)List_detach(&sem->waiting_descriptors,sem->waiting_descriptors.first);

        // prendo il primo PCB nella waiting list
        PCB* pcb_n = sem_des_ptr->descriptor->pcb;

        pcb_n->status=Ready;

        //tolgo il processo dalla waiting list del processo
        List_detach(&waiting_list, (ListItem*)pcb_n);

        //inserisco il processo nella ready list

        List_insert(&ready_list, ready_list.last, (ListItem*)pcb_n);

        // inserisco sem_des_ptr nella descriptor list dei semafori
        List_insert(&sem->descriptors, sem->descriptors.last, (ListItem*)sem_des_ptr);



    }

    running->syscall_retvalue=0;
    return;
}

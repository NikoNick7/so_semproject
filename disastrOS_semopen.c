#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"
#include "disastrOS_constants.h"

void internal_semOpen(){

    //prendo id e count dal PCB
    int sem_id=running->syscall_args[0];
    int sem_count=running->syscall_args[1];


    if(sem_count<0){

        running->syscall_retvalue=DSOS_COUNT_SEM_NEGATIVE;
        fprintf(stderr, "Error SEM_NEG: %d\n", DSOS_COUNT_SEM_NEGATIVE);

        return;
    }

    //prendo il semaforo con id sem_id dalla lista di semafori semaphores_list

    Semaphore* sem= SemaphoreList_byId(&semaphores_list,sem_id);

    if(!sem){
        sem= Semaphore_alloc(sem_id, sem_count);  //se sem non è stato allocato lo alloco
        if(!sem){
            running->syscall_retvalue=DSOS_SEM_NOTALLOC;
            fprintf(stderr, "Error SEM_NOTALLOC: %d\n", DSOS_SEM_NOTALLOC);
            return;
        }
        List_insert(&semaphores_list,semaphores_list.last,(ListItem*)sem);
    }

    //creo il descrittore per il sem e lo aggiungo nella lista
    // dei descrittori del processo

    SemDescriptor* sem_des= SemDescriptor_alloc(running->last_sem_fd,sem,running);

    if(!sem_des){
        running->syscall_retvalue=DSOS_SEM_DES_NOTALLOC;
        fprintf(stderr, "Error SEM_DES_NOTALLOC: %d\n", DSOS_SEM_DES_NOTALLOC);

        return;
    }

    running->last_sem_fd++;

    // alloco puntatore sem_des_ptr per il descrittore sem_des
    SemDescriptorPtr* sem_des_ptr = SemDescriptorPtr_alloc(sem_des);

    if(!sem_des_ptr){
        running->syscall_retvalue=DSOS_SEM_DES_PTR_NOTALLOC;
        fprintf(stderr, "Error SEM_DES_PTR_NOTALLOC: %d\n",DSOS_SEM_DES_PTR_NOTALLOC);
        return;
    }

    //inserisco sem_des nella lista dei descrittori dei sem del processo
    List_insert(&running->sem_descriptors, running->sem_descriptors.last, (ListItem*)sem_des);


    sem_des->ptr=sem_des_ptr;

    // inserisco sem_des_ptr nella lista dei descrittori ptr di sem
    List_insert(&sem->descriptors,sem->descriptors.last,(ListItem*)sem_des_ptr);

    // setto il valore di ritorno della syscall con fd del sem
    running->syscall_retvalue = sem_des->fd;
    return;


}
